# Instalación
ejecutar como root: (su / sudo su)

1. Actualizar sistema
```bash
apt update && apt upgrade
```

2. Instalar requisitos
```bash
sudo apt install apt-transport-https ca-certificates curl software-properties-common
```

3. Instalar credenciales para los paquetes de docker
```bash
apt install curl 
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

4. Agregar el repositorio ofical de Docker

```bash
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
```

5. Actualizar los paquetes
```bash
sudo apt update
```

6. Instalar Docker
```bash
sudo apt install docker-ce
```

7. Verificar que el Demonio esta corriendo
```bash
sudo systemctl status docker
```

# Ejecutar Docker sin ser sudo

1. Primero agregar al usuario de linux al grupo de Docker
```bash
sudo usermod -aG docker ${USER}
```

2. Volver a entrear a la consola con el usuario, para re-cargar su configuración
```bash
su - ${USER}
```

3. Confirmar que el usuario esta en el grupo de docker
```bash
id -nG
```

# Comandos docker

1. Para ver los comandos ejecutar 
```bash
docker
```

2. Información del docker instalado
```bash
docker info
```

# DockerHub
Repositorio de imagenes docker
* [DockerHub](https://hub.docker.com)